#!/bin/bash
# ff
#
ulimit -s 65536
. $PWD/000profile
RET=$?
if [ $RET -ne 0 ]
then
	echo "ERROR: RET=$RET PROFILE=$PWD/000profile. Exiting."
	exit $RET
fi
if [ "$#" != 4 ]
then 
	RET=1
	echo "ERROR: Usage $0 infile outfile csv_head csv_separator. Exiting."
	exit $RET
fi
INFILE=$1
OUTFILE=$2
CSV_HEAD=$3
CSV_SEPARATOR="$4"
#
if [ ! -r "$INFILE" ]
then
	RET=1
	echo "ERROR: File INFILE=$INFILE does not existst. Exiting"
	exit $RET
fi
#
> $OUTFILE
RET=$?
if [ $RET -ne 0 ]
then
	echo"ERROR: RET=$RET OUTFILE=$OUTFILE. Exiting."
	exit $RET
fi
#
awk -v V_SEPARATOR="$CSV_SEPARATOR" -v V_OUTFILE="$OUTFILE" -v V_CSV_HEAD="$CSV_HEAD" -f $HOMEDIR/130data2csv.awk  $INFILE
RET=$?
if [ $RET -ne 0 ]
then
	echo"ERROR: RET=$RET AWK=$HOMEDIR/130data2csv.awk. Exiting."
	exit $RET
fi
#
exit 0
#
