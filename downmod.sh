#!/bin/bash
ulimit -s 65536
. $PWD/000profile
RET=$?
if [ $RET -ne 0 ]
then
        echo "ERROR: RET=$RET PROFILE=$PWD/000profile. Exiting."
        exit $RET
fi
#
INDIR=downloads
OUTDIR=downloads
#
FILELIST=`ls $INDIR/*.*`
#
for i in `echo "$FILELIST"`
do
	echo "Working on  $i ..."
	INFILE="$i"
	BASEINFILE=`basename "$INFILE"`
	echo "BASEINFILE=$BASEINFILE"
	NEWNAME=${BASEINFILE/worldometers./ff.}
	echo "NEWNAME=$NEWNAME"
	OUTFILE="$OUTDIR"/"$NEWNAME"
	echo "OUTFILE=$OUTFILE"
	echo "Starting ... mv $INFILE $OUTFILE"
	mv $INFILE $OUTFILE
done
