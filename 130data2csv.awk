BEGIN {
	BEGTABLE=0
	ENDTABLE=0
	BEGROW=0
	BEGATTR=0
	ENDROW=0
	BEGROWTOT=0
	BEGROWTOT=0
	TR=""
	CELL=""
	TDCELL=""
	ACELL=""
	STRONGCELL=""
	SPANCELL=""
	TDSWITCH=0
	BUF18=""
#print "V_CSV_HEAD=" V_CSV_HEAD
}
func trunc18() {
        split(TR,a,V_SEPARATOR)
        BUF18=a[1]
        for(i=2;i<=18;i++) {
            BUF18=BUF18 V_SEPARATOR a[i]
        }
        #print BUF18
        TR = BUF18
}
{ 
# main beg
	if(BEGTABLE == 0) {
		# BEGTABLE is 0
		if(index($0, "<table id=\"main_table_countries_today\"") == 1 || index($0, "<table id=\"main_table_countries\"") == 1) {
			# print "$0=" $0
			BEGTABLE=1
			ENDTABLE=0
			BEGROW=0
			BEGATTR=0
			ENDROW=0
			BEGROWTOT=0
			BEGROWTOT=0
			TR=""
			CELL=""
			TDCELL=""
			ACELL=""
			STRONGCELL=""
			SPANCELL=""
			TDSWITCH=0
		}
	} else {
		# BEGTABLE is 1
		if(index($0, "</table>") == 1) {
			# print "$0=" $0
			BEGTABLE=0
			ENDTABLE=1
			BEGROW=0
			BEGATTR=0
			ENDROW=0
			BEGROWTOT=0
			BEGROWTOT=0
			TR=""
			CELL=""
			TDCELL=""
			ACELL=""
			STRONGCELL=""
			SPANCELL=""
			TDSWITCH=0
		} else {
			if(BEGROWTOT == 0) {
				#BEGROWTOT is 0
				if(index($0, "<tr class=\"total_row_world\"") == 1) {
					BEGROWTOT=1
					ENDROWTOT=0
					TR=""
					# print "$0=" $0
				}
			} else {
				#BEGROWTOT is 1
				if(index($0, "</tr>") == 1) {
					# end of row
					BEGROWTOT=0
					ENDROWTOT=1
					# print "$0=" $0
					trunc18()
					#print TR V_SEPARATOR V_CSV_HEAD 
					print TR V_SEPARATOR V_CSV_HEAD >> V_OUTFILE
				} else {
					# not end of row
					# print "$0=" $0
					if(index($0,"<td") == 1 && index($0,">") != 0) {
						gsub("<td.*>","",$0)
						TDCELL=$0
						TDSWITCH=0
						#print "TDCELL=" TDCELL
					}
					if(index($0,"<td") == 1 && index($0,">") == 0) {
						gsub("<td.*","",$0)
						TDCELL=$0
						TDSWITCH=1
						#print "TDCELL=" TDCELL
					}
					if(index($0,"<") == 0 && index($0,">") !=0 && TDSWITCH==1) {
						gsub("^.*>","",$0)
						TDCELL=$0
						TDSWITCH=0
						#print "TDCELL=" TDCELL
					}
					if(index($0,"<strong") == 1) {
						gsub("<strong.*>","",$0)
						STRONGCELL=$0
						#print "STRONGCELL=" STRONGCELL
					}
					if(index($0,"</td>") == 1) {
						#gsub("[ ]*","",TDCELL);
						#gsub("[ ]*","",STRONGCELL);
						gsub(/^[ \t]+|[ \t]+$/, "", TDCELL);
						gsub(/^[ \t]+|[ \t]+$/, "", STRONGCELL);
						if(TDCELL != "") { CELL=TDCELL; }
						if(STRONGCELL != "") { CELL=STRONGCELL; }
						#print "CELL=" CELL "#ff#"
						if(TR != "") {
							TR=TR V_SEPARATOR CELL
						} else {
							TR=CELL
						}
						#print "TR=" TR
						TDCELL=""
						STRONGCELL=""
						CELL=""
						TDSWITCH=0
					}
				}
			}
			if(BEGROW == 0) {
				#BEGROW is 0
				if(index($0, "<tr style=\"\"") == 1 || index($0, "<tr style=\"background-color:#F0F0F0\"") == 1 ) {
					BEGROW=1
					BEGATTR=0
					ENDROW=0
					TR=V_PREFIX
					# print "$0=" $0
				}
			} else {
				#BEGROW is 1
				if(index($0, "</tr>") == 1) {
					# end of row
					BEGROW=0
					BEGATTR=0
					ENDROW=1
					# print "$0=" $0
					trunc18()
					#print TR V_SEPARATOR V_CSV_HEAD 
					print TR V_SEPARATOR V_CSV_HEAD >> V_OUTFILE
				} else {
					# not end of row
					if(index($0, "<a class=\"mt_a\"") > 0) {
						BEGATTR=1
					}
					# print "$0=" $0
					if(BEGATTR==1 && index($0,"<td") == 1 && index($0,">") !=0 ) {
						gsub("<td.*>","",$0)
						TDCELL=$0
						TDSWITCH=0
						#print "TDCELL=" CELL
					}
					if(BEGATTR==1 && index($0,"<td") == 1 && index($0,">") == 0 ) {
						gsub("<td.*>","",$0)
						TDCELL=$0
						TDSWITCH=1
						#print "TDCELL=" CELL
					}
					if(BEGATTR==1 && index($0,"<") == 0 && index($0,">") != 0 && TDSWITCH == 1) {
						gsub("^.*>","",$0)
						TDCELL=$0
						TDSWITCH=1
						#print "TDCELL=" CELL
					}
					if(BEGATTR==1 && index($0,"<a") == 1) {
						gsub("<a.*>","",$0)
						ACELL=$0
						#print "ACELL=" ACELL
					}
					if(BEGATTR==1 && index($0,"<span") == 1) {
						gsub("<span.*>","",$0)
						SPANCELL=$0
						# print "SPANCELL=" SPANCELL
					}
					if(BEGATTR==1 && index($0,"</td>") == 1) {
						#gsub("[ ]*","",TDCELL);
						#gsub("[ ]*","",ACELL);
						#gsub("[ ]*","",SPANCELL);
						gsub(/^[ \t]+|[ \t]+$/, "", TDCELL);
						gsub(/^[ \t]+|[ \t]+$/, "", ACELL);
						gsub(/^[ \t]+|[ \t]+$/, "", SPANCELL);
						if(TDCELL != "") { CELL=TDCELL; }
						if(ACELL != "") { CELL=ACELL; }
						if(SPANCELL != "") { CELL=SPANCELL; }
						#print "CELL=" CELL "#ff#"
						if(TR != "") {
							TR=TR V_SEPARATOR CELL
						} else {
							TR=CELL
						}
						#print "TR=" TR
						TDCELL=""
						ACELL=""
						CELL=""
						SPANCELL=""
						TDSWITCH=0
					}
				}
			}
		}	
	}

#main end
}
END {
}
