BEGIN {
	# changes 1.6.2020: between "new deaths" and "total recovered" inserted one column!!!!
	#
	# 1 Country | 2 Total Cases | 3 New Cases | 4 Total Deaths |  5 New Deaths | 6 Total Recovered | 7 Active Cases | 8 Serious, Critical | 9 Total Cases/ 1M pop | 
	# 10 Deaths/ 1M pop  | 11 Total Tests | 12Tests/ 1M pop |  13..20 empty
	# 21 Timesatamp Formated | 22 Day Formated | 23 Time Formated | 24 Timestamp Unform | 25 Day Unform | 26 Time Unform
	# 16 Country Extension
	# 17 Growth Total Cases
	# 18 Growth Total Deaths 
	#
	indCountry=1
	indTotalCases=2
	indNewCases=3
	indTotalDeaths=4
	indNewDeaths=5
	indTotalRecovered=6
	indActiveCases=7
	indSeriousCritical=8
	indTotalCases1MPop=9
	indDeaths1MPop=10
	indTotalTests=11
	indTests1MPop=12
	indNull13=13
	indNull14=14
	indNull15=15
	indNull16=16
	indNull17=17
	indNull18=18
	indNull19=19
	indNull20=20
	#
	indTimeStampFormated=21
	#
	BUF=""
	NEWBUF=""
	COUNTRYEXT=""
	#
	a[0]=""	
	COLVAL[0]=""	
	currRowCell[0]=""
	prevRowCell[0]=""
	compNewCasesRow[0]=""
	compNewDeathsRow[0]=""
	compNewRecoveredRow[0]=""
	compNewActiveRow[0]=""
	compNewSeriousCriticalRow[0]=""
	compNewTotalCases1MPopRow[0]=""
}
{
	##########
	INPUTNF=NF
	INPUTROW=$0
	##########
	#print "Trace00.NR=" NR
	currRow=NR
	prevRow=NR-1
	#
	split($0,a);
	BUF=a[indCountry]
##ff##	for(i=2;i<=26;i++) {
##ff##		BUF=BUF V_CSV_SEPARATOR a[i]
##ff##	}
	for(i=2;i<=6;i++) {
		BUF=BUF V_CSV_SEPARATOR a[i]
	}
	for(i=8;i<=20;i++) {
		BUF=BUF V_CSV_SEPARATOR a[i]
	}
	BUF=BUF V_CSV_SEPARATOR ""
	for(i=21;i<=26;i++) {
		BUF=BUF V_CSV_SEPARATOR a[i]
	}
	#print "Trace02.BUF=" BUF
	#
	split(BUF,COLVAL)
        for(i=2;i<=12;i++) {
                gsub("+","",COLVAL[i]);
                gsub(",","",COLVAL[i]);
                # gsub("\\.","",COLVAL[i]);
                if(COLVAL[i]=="") { COLVAL[i]=0; }
	}
	#print "Trace05.COLVAL["indSeriousCritical"]=" COLVAL[indSeriousCritical]
	#print "Trace05.COLVAL["indTotalCases1MPop"]=" COLVAL[indTotalCases1MPop]
	#
	if(currRow>1) {
		for(i=1;i<=26;i++) {
			currRowCell[i] = COLVAL[i]
		}
		compNewCases=currRowCell[indTotalCases]-prevRowCell[indTotalCases]
		compNewDeaths=currRowCell[indTotalDeaths]-prevRowCell[indTotalDeaths]
		compNewRecovered=currRowCell[indTotalRecovered]-prevRowCell[indTotalRecovered]
		compNewActive=currRowCell[indActiveCases]-prevRowCell[indActiveCases]
		compNewSeriousCritical=currRowCell[indSeriousCritical]-prevRowCell[indSeriousCritical]
		compNewTotalCases1MPop=currRowCell[indTotalCases1MPop]-prevRowCell[indTotalCases1MPop]
		#print "Trace10.currRowCell["indSeriousCritical"]=" currRowCell[indSeriousCritical]
		#print "Trace10.prevRowCell["indSeriousCritical"]=" prevRowCell[indSeriousCritical]
		#print "Trace10.compNewSeriousCritical=" compNewSeriousCritical
		#print "Trace10.currRowCell["indTotalCases1MPop"]=" currRowCell[indTotalCases1MPop]
		#print "Trace10.prevRowCell["indTotalCases1MPop"]=" prevRowCell[indTotalCases1MPop]
		#print "Trace10.compNewTotalCases1MPop=" compNewTotalCases1MPop
	} else {
		for(i=1;i<=26;i++) {
			currRowCell[i] = COLVAL[i]
			prevRowCell[i] = ""
		}
		compNewCases=0
		compNewDeaths=0
		compNewRecovered=0
		compNewActive=0
		compNewSeriousCritical=0
		compNewTotalCases1MPop=0
	}
	compNewCasesRow[currRow]=compNewCases
	compNewDeathsRow[currRow]=compNewDeaths
	compNewRecoveredRow[currRow]=compNewRecovered
	compNewActiveRow[currRow]=compNewActive
	compNewSeriousCriticalRow[currRow]=compNewSeriousCritical
	compNewTotalCases1MPopRow[currRow]=compNewTotalCases1MPop
	#print "Trace20.compNewSeriousCritical=" compNewSeriousCritical
	#print "Trace20.compNewTotalCases1MPop=" compNewTotalCases1MPop
	if(currRow>2) {
		#
		d=compNewCasesRow[prevRow]
		if(d==0) d=1;
		growthCases=compNewCasesRow[currRow]/d
		#
		d=compNewDeathsRow[prevRow]
		if(d==0) d=1;
		growthDeaths=compNewDeathsRow[currRow]/d
		#
		d=compNewRecoveredRow[prevRow]
		if(d==0) d=1;
		growthRecovered=compNewRecoveredRow[currRow]/d
		#
		d=compNewActiveRow[prevRow]
		if(d==0) d=1;
		growthActive=compNewActiveRow[currRow]/d
		#
		d=compNewSeriousCriticalRow[prevRow]
		if(d==0) d=1;
		growthSeriousCritical=compNewSeriousCriticalRow[currRow]/d
		#
		d=compNewTotalCases1MPopRow[prevRow]
		if(d==0) d=1;
		growthTotalCases1MPop=compNewTotalCases1MPopRow[currRow]/d
	} else {
		growthCases=0
		growthDeaths=0
		growthRecovered=0
		growthActive=0
		growthSeriousCritical=0
		growthTotalCases1MPop=0
	}
	for(i=1;i<=26;i++) {
		prevRowCell[i] = COLVAL[i]
	}
	if(currRow>1) {
		COLVAL[indNewCases]=compNewCases
		COLVAL[indNewDeaths]=compNewDeaths
	}
	COUNTRYEXT=COLVAL[indCountry]
        gsub(" ","",COUNTRYEXT);
        gsub("\\.","_",COUNTRYEXT);
        sub("World","WORLD",COUNTRYEXT);
        sub("Total:","WORLD",COUNTRYEXT);
	NEWBUF=COLVAL[indCountry]
	for(i=2;i<=26;i++) {
		NEWBUF=NEWBUF V_CSV_SEPARATOR COLVAL[i];
	}
	NEWBUF=NEWBUF V_CSV_SEPARATOR COUNTRYEXT
	NEWBUF=NEWBUF V_CSV_SEPARATOR growthCases
	NEWBUF=NEWBUF V_CSV_SEPARATOR growthDeaths
	NEWBUF=NEWBUF V_CSV_SEPARATOR growthRecovered
	NEWBUF=NEWBUF V_CSV_SEPARATOR growthActive
	NEWBUF=NEWBUF V_CSV_SEPARATOR growthSeriousCritical
	NEWBUF=NEWBUF V_CSV_SEPARATOR growthTotalCases1MPop
	NEWBUF=NEWBUF V_CSV_SEPARATOR compNewRecovered
	NEWBUF=NEWBUF V_CSV_SEPARATOR compNewActive
	NEWBUF=NEWBUF V_CSV_SEPARATOR compNewSeriousCritical
	NEWBUF=NEWBUF V_CSV_SEPARATOR compNewTotalCases1MPop
	#
	#print "Trace90.COUNTRYEXT=" COUNTRYEXT
	#print "Trace90.growthCases=" growthCases
	#print "Trace90.growthDeaths=" growthDeaths
	#print "Trace90.growthRecovered=" growthRecovered
	#print "Trace90.growthActive=" growthActive
	#print "Trace90.growthSeriousCritical=" growthSeriousCritical
	#print "Trace90.growthTotalCases1MPop=" growthTotalCases1MPop
	#print "Trace90.compNewRecovered=" compNewRecovered
	#print "Trace90.compNewActive=" compNewActive
	#print "Trace90.compNewSeriousCritical=" compNewSeriousCritical
	#print "Trace90.compNewTotalCases1MPop=" compNewTotalCases1MPop
	if(INPUTNF!=26) {
		print INPUTROW
	} else {
		print NEWBUF
	}
}
END {
}
