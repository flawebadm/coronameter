#!/bin/bash
#set -x
#
ulimit -s 65536
HOST="sv50.byethost50.org"
USER="flavioby"
PASSWD="By.1234.__!!__"
FILELIST=`ls htdocsC/*.html`
MPUT=""
MREN=""
for LOCALFILE in `echo "$FILELIST"`
do 
#
	PUTFILE=`basename "$LOCALFILE"`
	MPUT="$MPUT\nput $LOCALFILE $PUTFILE.temp"
	MREN="$MREN\nrename $PUTFILE.temp $PUTFILE"
#
done
MPUTF=`printf "$MPUT"`
MRENF=`printf "$MREN"`
#
ftp -nv $HOST <<END_SCRIPT
quote USER $USER
quote PASS $PASSWD
passive
ascii
prompt
cd public_html
#cd flajs.66ghz.com
cd flajs.22web.org
"$MPUTF"
"$MRENF"
quit
END_SCRIPT
