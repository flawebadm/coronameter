#
BEGIN {
	# single quote
	SQ="'"
	# plus sign
	PL=""
}
{
	split($0,a);
	if(a[1] == "World" || a[1] == "Total:") {
		a[1]="WORLD"
	}
	if(FNR==1) {
		print "</td></tr>"
		print "</table> \
		\n<!-- BEG NEW CHART -->  \
		\n<table> \
		\n<tr><td class=\"tleft\" colspan=\"4\"><big><strong>" a[1] "</strong></big> Trends</td></tr> \
		\n<tr> \
		\n	<td class=\"tleft\"><div id=\"total_cases_chart\"></div></td> \
		\n	<td class=\"tleft\"><div id=\"total_deaths_chart\"></div></td> 		 \
		\n	<td class=\"tleft\"><div id=\"total_recovered_chart\"></div></td> 		 \
		\n	<td class=\"tleft\"><div id=\"new_cases_chart\"></div></td> 		 \
		\n</tr> \
		\n<tr> \
		\n	<td class=\"tleft\"><div id=\"active_cases_chart\"></div></td> \
		\n	<td class=\"tleft\"><div id=\"serious_critical_chart\"></div></td> 		 \
		\n	<td class=\"tleft\"><div id=\"total_cases_1m_pop_chart\"></div></td> 		 \
		\n	<td class=\"tleft\"><div id=\"new_deaths_chart\"></div></td> 		 \
		\n</tr> \
		\n</table> \
		\n<table> \
		\n<!-- END NEW CHART --> 	 \
		\n<tr><td class=\"tleftnoborder\">  \
		\n<table> \
		\n<tr> \
		\n<td class=\"tleft\" colspan=\"12\"><big><strong>" a[1] "</strong></big> Collected Data</td> \
		\n</tr> \
		\n<tr> \
		\n<th width=\"1%\" class=\"tleft\">Datum</th> \
		\n<th width=\"1%\">Total Cases</th> \
		\n<th width=\"1%\">New Cases</th> \
		\n<th width=\"1%\">Total Deaths</th> \
		\n<th width=\"1%\">New Deaths</th> \
		\n<th width=\"1%\">Total Recovered</th> \
		\n<th width=\"1%\">Active Cases</th> \
		\n<th width=\"1%\">Serious, Critical</th> \
		\n<th width=\"1%\">Total Cases/ 1M pop</th> \
		\n<th width=\"1%\">Deaths/ 1M pop</th> \
		\n<th width=\"1%\">Total Tests</th> \
		\n<th width=\"1%\">Tests/ 1M pop</th> \
		\n</tr>"
	} 
	#
	if(a[1] == "WORLD") {
		TTAG="th"
	} else {
		TTAG="td"
	}
	for(i=2;i<=12;i++) {
		if(a[i] == 0) { 
			a[i] = ""; 
		}
		else {
			if(i==3 || i==5) { 
				PL="+"
			} else {
				PL=""
			}
			if(index(a[i],".") == 0) {
				a[i] = sprintf("%'" SQ "'" PL "9.0f", a[i]);
			} else {
				a[i] = sprintf("%'" SQ "'" PL "9.2f", a[i]);
			}
		}
	}
	#
	for(i=1;i<=12;i++) {
		b[i] = sprintf("<%s>%s</%s>", TTAG, a[i], TTAG)
	}
	b[1] = sprintf("<%s class=\"tleft\">%s</%s>", TTAG, a[21], TTAG)
	if(a[3] != "") {
		b[3]= sprintf("<%s style=\"background-color:#ffeeaa;\"><strong>%s</strong></%s>", TTAG, a[3], TTAG)
	}
	if(a[5] != "") {
		b[5]= sprintf("<%s style=\"background-color:red; color:#fff\"><strong>%s</strong></%s>", TTAG, a[5], TTAG)
	}
	#
	print "<tr>"
	for(i=1; i<=12; i++) {
		print b[i];
	}
	print "</tr>"
}
END {
	print "<tr>"
	print "<td class=\"tleft\" colspan=\"12\">"
	print "<a href=\"#top\">Back to top of page</a>|<a href=\"#data_sources\">Data sources</a>|<a href=\"#explanation\">Explanation</a>|<a href=\"#disclaimer\">Disclaimer</a>"
	print "</td>"
	print "</tr>"
	print "</table>"
}
