#
BEGIN {
        # single quote
        SQ="'"
        # plus sign
        PL=""
}
{
	split($0,a)
	if(a[1] == "World" || a[1] == "Total:") {
		a[1] = "WORLD"
	}
	#print "<!-- TRACE 100: a1=" a[1] " -->"
	#print "<!-- TRACE 100: a2=" a[2] " -->"
	#print "<!-- TRACE 100: a3=" a[3] " -->"
	if(FNR==1) {
		if(V_FIRST_FILE==1) {
			print "\n<!-- BEG NEW CHART -->\
			\n</td></tr> \
			\n</table> \
			\n<table>\
			\n<tr>\
			\n	<th class=\"tleft\" colspan=\"4\">Top 10 Countries</th>\
			\n</tr>\
			\n<tr>\
			\n	<td class=\"tleft\"><div id=\"" a[25] "chart_total_cases\"</div></td>\
			\n	<td class=\"tleft\"><div id=\"" a[25] "chart_total_deaths\"></div></td>\
			\n	<td class=\"tleft\"><div id=\"" a[25] "chart_total_recovered\"></div></td>\
			\n	<td class=\"tleft\"><div id=\"" a[25] "chart_deaths_1m_pop\"></div></td>\
			\n</tr><tr> \
			\n	<td class=\"tleft\"><div id=\"" a[25] "chart_active_cases\"></div></td>\
			\n	<td class=\"tleft\"><div id=\"" a[25] "chart_serious_critical\"></div></td>\
			\n	<td class=\"tleft\"><div id=\"" a[25] "chart_total_cases_1m_pop\"></div></td>\
			\n	<td class=\"tleft\"><div id=\"" a[25] "chart_tests_1m_pop\"></div></td>\
			\n</tr>\
			\n</table>\
			\n<table> \
			\n<!-- END NEW CHART -->"
		}
		print "<tr><td class=\"tleftnoborder\"> \
		\n<table> \
		\n<tr> \
		\n<td class=\"tleft\" colspan=\"12\"><big><strong>" a[22] "</strong></big> Data collected on " a[21] "</td> \
		\n</tr> "
		print "\n<tr> \
		\n<th width=\"1%\" class=\"tleft\">Country</th> \
		\n<th width=\"1%\">Total Cases</th> \
		\n<th width=\"1%\">New Cases</th> \
		\n<th width=\"1%\">Total Deaths</th> \
		\n<th width=\"1%\">New Deaths</th> \
		\n<th width=\"1%\">Total Recovered</th> \
		\n<th width=\"1%\">Active Cases</th> \
		\n<th width=\"1%\">Serious, Critical</th> \
		\n<th width=\"1%\">Total Cases/ 1M pop</th> \
		\n<th width=\"1%\">Deaths/ 1M pop</th> \
		\n<th width=\"1%\">Total Tests</th> \
		\n<th width=\"1%\">Tests/ 1M pop</th> \
		\n</tr>"
	} 
#new# ####################################################################################
	COUNTRY=a[27]
	countryfile=V_LEFTDAY "." COUNTRY ".html"
	#
	if(a[1] == "WORLD") {
		TTAG="th"
	} else {
		TTAG="td"
	}
	for(i=2;i<=12;i++) {
		if(a[i] == 0) { 
			a[i] = ""; 
		} else {
			if(i==3 || i==5) { 
				PL="+"
			} else {
				PL=""
			}
			if(index(a[i],".") == 0) {
				a[i] = sprintf("%'" SQ "'" PL "9.0f", a[i]);
			} else {
				a[i] = sprintf("%'" SQ "'" PL "9.2f", a[i]);
			}
		}
	}
	#print "<!-- TRACE 200: a1=" a[1] " -->"
	#print "<!-- TRACE 200: a2=" a[2] " -->"
	#print "<!-- TRACE 200: a3=" a[3] " -->"
	#
	for(i=1;i<=12;i++) {
		b[i] = sprintf("<%s>%s</%s>", TTAG, a[i], TTAG)
	}
	b[1] = sprintf("<%s class=\"tleft\"><a href=\"%s\">%s</%s></a>", TTAG, countryfile, a[1], TTAG)
	if(a[3] != "") {
		b[3]= sprintf("<%s style=\"background-color:#ffeeaa;\"><strong>%s</strong></%s>", TTAG, a[3], TTAG)
	}
	if(a[5] != "") {
		b[5]= sprintf("<%s style=\"background-color:red; color:#fff\"><strong>%s</strong></%s>", TTAG, a[5], TTAG)
	}
	#print "<!-- TRACE 300: b1=" b[1] " -->"
	#print "<!-- TRACE 300: b2=" b[2] " -->"
	#print "<!-- TRACE 300: b3=" b[3] " -->"
	#
	print "<tr>"
	for(i=1; i<=12; i++) {
		print b[i];
	}
	print "</tr>"
#new# ####################################################################################
}
END {
	print "<tr>"
	print "<td class=\"tleft\" colspan=\"12\">"
	print "<a href=\"#top\">Back to top of page</a>|<a href=\"#data_sources\">Data sources</a>|<a href=\"#explanation\">Explanation</a>|<a href=\"#disclaimer\">Disclaimer</a>"
	print "</td>"
	print "</tr>"
	print "</table>"
}
