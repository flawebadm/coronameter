#!/bin/bash
# ff
#
ulimit -s 65536
. $PWD/000profile
RET=$?
if [ $RET -ne 0 ]
then
	echo "ERROR: LINENO=$LINENO RET=$RET PROFILE=$PWD/000profile. Exiting."
	exit $RET
fi
# # # # # # # #
# # # # # # # #
#
# DAY
#
# # # # # # # #
# # # # # # # #
rm -f $TEMPTDIR/$SOURCENAME.day.prepared.*
sort  -t"$CSV_SEPARATOR" -k21,21r -k2,2nr $PREPAREDIR/$SOURCENAME.country.day.prepared.* | awk -F"$CSV_SEPARATOR" -v V_LEFT="$TEMPDIR/$SOURCENAME.day.prepared." '{ F=V_LEFT $25; print $0 >> F; }'
RET=$?
if [ $RET -ne 0 ]
then
	echo "ERROR: LINENO=$LINENO RET=$RET sort/awk. Exiting."
	exit $RET
fi
mv -v $TEMPDIR/$SOURCENAME.day.prepared.* $PREPAREDIR
RET=$?
if [ $RET -ne 0 ]
then
	echo "ERROR: LINENO=$LINENO RET=$RET mv. Exiting."
	exit $RET
fi
