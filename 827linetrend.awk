BEGIN {
	ELE=""
        BUF=""
	IND=0
}
{
        split($0,a);
        COLVAL[1]=a[1]
        for(i=2;i<=12;i++) {
                COLVAL[i]=a[i]
                gsub("+","",COLVAL[i]);
                gsub(",","",COLVAL[i]);
                # gsub("\\.","",COLVAL[i]);
                if(COLVAL[i]=="") { COLVAL[i]=0; }
        }
	# [0,5],    [1, 1],   [2, 2],  [3, 3]
	ELE = "[" IND++ ", " COLVAL[V_COLNUM1] "]"
	if(BUF != "") {
		BUF = BUF ", " ELE
	} else {
		BUF = ELE
	} 
}
END {
	print "<script type=\"text/javascript\">"
	print "google.charts.load(\"current\", {packages: [\"corechart\", \"line\"]});"
	print "google.charts.setOnLoadCallback(drawTrendlines);"
	print "function drawTrendlines() {"
	print "      var data = new google.visualization.DataTable();"
	print "      data.addColumn(\"number\", \"Days\");"
	print "      data.addColumn(\"number\", \"" V_COLTITLE1 "\");"
	print "      data.addRows(["
	print BUF
	print "      ]);"
	print "      var options = {"
	print "	width: 350,"
	print "	height: 300,"
	print "	legend: { position: \"top\" },"
	print "	colors: [\"blue\"],"
	print "        trendlines: {"
	print "          0: {type: \"" V_TRENDTYPE "\", color: \"#333\", opacity: .3}"
	print "          //0: {type: \"exponential\", color: \"#333\", opacity: .3}"
	print "          //0: {type: \"linear\", color: \"#111\", opacity: .3}"
	print "        }"
	print "      };"
	print "      var chart = new google.visualization.LineChart(document.getElementById(\"" V_CHARTDIV "\"));"
	print "      chart.draw(data, options);"
	print "    }"
	print "</script>"
}
