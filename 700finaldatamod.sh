#!/bin/bash
# ff
#
ulimit -s 65536
. $PWD/000profile
RET=$?
if [ $RET -ne 0 ]
then
	echo "ERROR: LINENO=$LINENO RET=$RET PROFILE=$PWD/000profile. Exiting."
	exit $RET
fi
rm -f $TEMPDIR/*.temp
# # # # # # # #
# # # # # # # #
#
# COUNTRY
#
# # # # # # # #
# # # # # # # #
FILELIST=""
#
FILELIST=`ls -Xr $PREPAREDIR/$SOURCENAME.country.day.prepared.*`
RET=$?
if [ $RET -ne 0 ]
then
	echo "ERROR: LINENO=$LINENO RET=$RET FILELIST=$FILELIST. Exiting."
	exit $RET
else
	FILECOUNT=`echo "$FILELIST" | grep -v "^$" | wc -l`
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO Country RET=$RET FILECOUNT=$FILECOUNT. Exiting."
		exit $RET
	fi
	#
	#
	for i in `echo "$FILELIST"`
	do
		echo "Working on $i ... "
		INFILE="$i"
		INBASE=$(basename -- "$INFILE")
		INEXTENSION="${INBASE##*.}"
		MODYINFILE="$TEMPDIR/$INBASE.mody.temp"
		SORTINFILE="$TEMPDIR/$INBASE.sort.temp"
		#
		awk -v V_CSV_SEPARATOR="$CSV_SEPARATOR" -F"$CSV_SEPARATOR" -f 720finalcountry.awk "$INFILE" > "$MODYINFILE"
		RET=$?
		if [ $RET -ne 0 ]
		then
			echo "ERROR: LINENO=$LINENO RET=$RET sort INFILE=$INFILE MODYINFILE=$MODYINFILE SORTINFILE=$SORTINFILE. Exiting."
			exit $RET
		fi
		sort -r --field-separator="$CSV_SEPARATOR" --key=24 "$MODYINFILE" > "$SORTINFILE"
		RET=$?
		if [ $RET -ne 0 ]
		then
			echo "ERROR: LINENO=$LINENO RET=$RET sort INFILE=$INFILE SORTINFILE=$SORTINFILE. Exiting."
			exit $RET
		fi
		cp "$SORTINFILE" "$INFILE"
		RET=$?
		if [ $RET -ne 0 ]
		then
			echo "ERROR: LINENO=$LINENO RET=$RET cp INFILE=$INFILE SORTINFILE=$SORTINFILE. Exiting."
			exit $RET
		fi
		#
		rm -f "$SORTINFILE"
		rm -f "$MODYINFILE"
	#
	done
fi
# # # # # # # #
# # # # # # # #
#
# DAY
#
# # # # # # # #
# # # # # # # #
./740finalday.sh
RET=$?
if [ $RET -ne 0 ]
then
	echo "ERROR: LINENO=$LINENO RET=$RET 740finalday.sh. Exiting."
exit $RET
fi

