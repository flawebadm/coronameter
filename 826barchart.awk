BEGIN {
	BUF=""
}
{
	split($0,a);
	COLVAL[1]=a[1]
	for(i=2;i<=12;i++) {
                COLVAL[i]=a[i]
                gsub("+","",COLVAL[i]);
                gsub(",","",COLVAL[i]);
                # gsub("\\.","",COLVAL[i]);
                if(COLVAL[i]=="") { COLVAL[i]=0; }
	}
	#  ["Italy", 9999, "stroke-color: #000000; stroke-width: 2; fill-color: #e5e4e2"],
	BUF=BUF "," "[" "\"" COLVAL[V_COLNUM1] "\"" "," COLVAL[V_COLNUM2] "," "\"stroke-color: #000000; stroke-width: 2; fill-color: #e5e4e2\"" "]"
}
END {
	print "<script type=\"text/javascript\">"
	print "    google.charts.load(\"current\", {packages:[\"corechart\"]});"
	print "    google.charts.setOnLoadCallback(drawChart);"
	print "    function drawChart() {"
	print "      var data = google.visualization.arrayToDataTable(["
	print "        [\"" V_COLTITLE1 "\", \"" V_COLTITLE2 "\", { role: \"style\" } ]"
	print BUF
	print "      ]);"
	print ""
	print "      var view = new google.visualization.DataView(data);"
	print "      view.setColumns([0, 1,"
	print "                       { calc: \"stringify\","
	print "                         sourceColumn: 1,"
	print "                         type: \"string\","
	print "                         role: \"annotation\" },"
	print "                       2]);"
	print ""
	print "      var options = {"
	print "        title: \"" V_CHARTTITLE "\","
	print "        width: 350,"
	print "        height: 300,"
	print "        bar: {groupWidth: \"95%\"},"
	print "        legend: { position: \"none\" },"
	print "      };"
	print "      var chart = new"
	print "      google.visualization.BarChart(document.getElementById(\"" a[25] V_CHARTDIV "\"));" 
	print "      chart.draw(view, options);"
	print "  }"
	print "</script>"
}
