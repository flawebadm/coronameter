#!/bin/bash
# ff
#
ulimit -s 65536
. $PWD/000profile
RET=$?
if [ $RET -ne 0 ]
then
	echo "ERROR: LINENO=$LINENO RET=$RET PROFILE=$PWD/000profile. Exiting."
	exit $RET
fi
NPARS=$#
# defaults
CSV_SEPARATOR="|"
FORCE="noforce"
#
if [ $NPARS -gt 1 ]
then 
	RET=1
	echo "ERROR: LINENO=$LINENO RET=$RET Usage $0 [ noforce|force ]. Default noforce. Exiting."
	exit $RET
else
	if [ $NPARS -eq 1 ]
	then
		FORCE="$1"
		if [ ! "$FORCE" = "force" ] && [ ! "$FORCE" = "noforce" ] 
		then
			RET=1
			echo "ERROR: LINENO=$LINENO Unknown value FORCE=$FORCE". Exiting.
			exit $RET
		fi 
	else
		FORCE="noforce"
	fi
fi
#
#
#
FILELIST=""
#
if [ "$FORCE" = "noforce" ]
then
	FILELIST=`ls -X $DOWNLOADSDIR/$SOURCENAME.info.*`
	# do not ignore errors
	RET=$?
else
	FILELIST=`ls -X $DOWNLOADSDIR/$SOURCENAME.info.* $DOWNLOADSDIR/$SOURCENAME.prepared.info.*`
	# ignore errors
	RET=0
fi
if [ $RET -ne 0 ]
# BEGIN IF RET FILELIST
then
	echo "ERROR: LINENO=$LINENO RET=$RET FORCE=$FORCE FILELIST=$FILELIST. Exiting."
	exit $RET
else
FILECOUNT=`echo "$FILELIST" | grep -v "^$" | wc -l`
RET=$?
if [ $RET -ne 0 ]
then
	echo "ERROR: LINENO=$LINENO RET=$RET FILECOUNT=$FILECOUNT $FILELIST. Exiting."
	exit $RET
fi
#
echo "FILECOUNT $FILECOUNT"
for i in `echo "$FILELIST" | grep -v "^$"`
do
	# BEGIN DO FILELIST 
	export INFOFILE=$i
	#ff#
	# echo "Working on $INFOFILE"
	# echo "Sourcing ..."
	#
	CSV_HEAD=""
	#
	. $INFOFILE
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET INFOFILE=$INFOFILE. Exiting."
		exit $RET
	fi
	#
	if [ "x$CSV_HEAD" = "x" ]
	then
		CSV_HEAD="$DATUMF$CSV_SEPARATOR$DATUMDAYF$CSV_SEPARATOR$DATUMTIMEF$CSV_SEPARATOR$DATUM$CSV_SEPARATOR$DATUMDAY$CSV_SEPARATOR$DATUMTIME"
		echo "CSV_HEAD=\"$DATUMF$CSV_SEPARATOR$DATUMDAYF$CSV_SEPARATOR$DATUMTIMEF$CSV_SEPARATOR$DATUM$CSV_SEPARATOR$DATUMDAY$CSV_SEPARATOR$DATUMTIME\"" >> "$INFOFILE"
		RET=$?
		if [ $RET -ne 0 ]
		then
			echo "ERROR: LINENO=$LINENO RET=$RET INFOFILE=$INFOFILE. Exiting."
			exit $RET
		fi
	fi
	#
	#ff#
	#echo "CSV_HEAD=$CSV_HEAD"
	#
	export PREPINFOFILE="$DOWNLOADSDIR/$SOURCENAME.prepared.info.$DATUM"
	export PUBFILE="$DOWNLOADSDIR/$SOURCENAME.pub.$DATUM"
	export PREPFILE="$PREPAREDIR/$SOURCENAME.prepared.$DATUM"
	export PREPDAYFILE="$PREPAREDIR/$SOURCENAME.day.prepared.$DATUMDAY"
	#ff#
	#echo "PREPDAYFILE=$PREPDAYFILE"
	export TEMPFILE1="$TEMPDIR/$SOURCENAME.work.$DATUM.temp1"
	export TEMPFILE2="$TEMPDIR/$SOURCENAME.work.$DATUM.temp2"
	export TEMPFILE3="$TEMPDIR/$SOURCENAME.work.$DATUM.temp3"
	export TEMPFILE4="$TEMPDIR/$SOURCENAME.work.$DATUM.temp4"
	#
	if [ "$FORCE" = "noforce" ]
	then
		if [ -r "$PREPFILE" ]
		then
			# echo "Prepared file PREPFILE=$PREPFILE exists. Ignoring with FORCE=$FORCE. Use force to prepare it again. ..."
			continue
		fi
	fi
	#
	echo "Preparing `date`... $INFOFILE"
	> $PREPFILE
	> $TEMPFILE1
	> $TEMPFILE2
	> $TEMPFILE3
	> $TEMPFILE4
	#
	cp -p "$INFOFILE" "$TEMPFILE3"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET cp INFOFILE=$INFOFILE TEMPFILE3=$TEMPFILE3. Exiting."
		exit $RET
	fi
	#
	cat $PUBFILE | sed 's/</\
</g' >> $TEMPFILE1
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET sed PUBFILE=$PUBFILE. Exiting."
		exit $RET
	fi
	# remove html comments
	cat $TEMPFILE1 | sed -e :a -re 's/<!--.*?-->//g;/<!--/N;//ba' > $TEMPFILE2
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET html comments TEMPFILE1=$TEMPFILE1 TEMPFILE2=$TEMPFILE2. Exiting."
		exit $RET
	fi
	#
	# prepare csv data
	#
	$HOMEDIR/120prepare_csv.sh "$TEMPFILE2" "$PREPFILE" "$CSV_HEAD" "$CSV_SEPARATOR"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET TEMPFILE2=$TEMPFILE2 PREPFILE=$PREPFILE CSV_HEAD=$CSV_HEAD $HOMEDIR/120prepare_csv.sh. Exiting."
		exit $RET
	fi
	#
	# cleansing
	#
	awk -v V_CSV_SEPARATOR="$CSV_SEPARATOR" -F"$CSV_SEPARATOR" -f $HOMEDIR/datamod.awk "$PREPFILE" > "$TEMPFILE4"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET. Exiting."
		exit $RET
	fi
	cp -p "$TEMPFILE4" "$PREPFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET. Exiting."
		exit $RET
	fi
	#
	cp -p "$PREPFILE" "$PREPDAYFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET cp PREPFILE=$PREPFILE PREPDAYFILE=$PREPDAYFILE. Exiting."
		exit $RET
	fi
	#
	# info file!
	#
	cp -p "$TEMPFILE3" "$PREPINFOFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET cp TEMPFILE3=$TEMPFILE3 PREPINFOFILE=$PREPINFOFILE. Exiting."
		exit $RET
	fi
	if [ "$FORCE" = "force" ]
	then
		cp -p "$TEMPFILE3" "$INFOFILE"
		RET=$?
		if [ $RET -ne 0 ]
		then
			echo "ERROR: LINENO=$LINENO RET=$RET cp TEMPFILE3=$TEMPFILE3 INFOFILE=$INFOFILE. Exiting."
			exit $RET
		fi
	fi
	#
	rm -f "$TEMPFILE1"
	rm -f "$TEMPFILE2"
	rm -f "$TEMPFILE3"
	rm -f "$TEMPFILE4"
##ff##	#
##ff##	# prepare html data
##ff##	#
##ff##	$HOMEDIR/140prepare_html.sh "$PREPFILE" "$HTMLFILE" "$CSV_SEPARATOR"
##ff##	RET=$?
##ff##	if [ $RET -ne 0 ]
##ff##	then
##ff##		echo "ERROR: RET=$RET $HOMEDIR/140prepare_html.sh. Exiting."
##ff##		exit $RET
##ff##	fi
	#
# END DO FILELIST
done
#
# country files
#
#
FILELIST=""
#
FILELIST=`ls -X $PREPAREDIR/$SOURCENAME.day.prepared.*`
RET=$?
if [ $RET -ne 0 ]
then
	echo "ERROR: LINENO=$LINENO Country RET=$RET FORCE=$FORCE FILELIST=$FILELIST. Exiting."
	exit $RET
else
FILECOUNT=`echo "$FILELIST" | grep -v "^$" | wc -l`
RET=$?
if [ $RET -ne 0 ]
then
	echo "ERROR: LINENO=$LINENO Country RET=$RET FILECOUNT=$FILECOUNT. Exiting."
	exit $RET
fi
#
LEFTDAY="$PREPAREDIR/$SOURCENAME.country.day.prepared"
COUNTRY_LIST=`cat $LEFTDAY.* | awk -F"$CSV_SEPARATOR" '{COUNTRY=$1;  gsub(" ","",COUNTRY); gsub(/\./,"_",COUNTRY); sub("World","WORLD",COUNTRY); sub("Total:","WORLD",COUNTRY); print COUNTRY; }' | grep -v "^$" | sort -u`
RET=$?
if [ $RET -ne 0 ]
then
	echo "ERROR: LINENO=$LINENO RET=$RET COUNTRY_LIST=$COUNTRY_LIST. Exiting."
	exit $RET
fi
# echo "$COUNTRY_LIST" 
for c in `echo "$COUNTRY_LIST"`
do 
	> $LEFTDAY.$c
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET c=$c LEFTDAY=$LEFTDAY. Exiting."
		exit $RET
	fi
done
#
for i in `echo "$FILELIST"`
do
	echo "Country: Working on $i ..."
	INFILE="$i"
	awk -v V_FIRST="$FIRST" -v V_LEFTDAY="$LEFTDAY" -v V_SEPARATOR="$CSV_SEPARATOR" -F"$CSV_SEPARATOR" -f 132country.awk "$INFILE"
	#
done
#
#
fi
# ###################
# END IF RET FILELIST
fi
