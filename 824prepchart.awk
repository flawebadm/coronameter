BEGIN {
	ACTIVE_BUF=""
	TOTAL_BUF=""
}
{
	split($0,a)
	a[0]=$0;
	ROWCNT++;
	for(i=2;i<=12;i++) {
		COLVAL[i]=a[i]
		gsub("+","",COLVAL[i]);
		gsub(",","",COLVAL[i]);
		gsub("\\.","",COLVAL[i]);
		if(COLVAL[i]=="") { COLVAL[i]=0; }
	}
	ACTIVE_CASES=COLVAL[7];
	TOTAL_DEATH=COLVAL[4]
	TOTAL_RECOVERED=COLVAL[6]
	CLOSED_CASES=TOTAL_DEATH+TOTAL_RECOVERED
	PERC_DEATH=0
	PERC_RECOVERED=0
	if(CLOSED_CASES != 0 ) {
		PERC_DEATH=100*TOTAL_DEATH/CLOSED_CASES
		PERC_RECOVERED=100*TOTAL_RECOVERED/CLOSED_CASES
	}
	TOTAL_BUF=TOTAL_BUF "," "[" "\"" a[22] "\"" "," ACTIVE_CASES "," TOTAL_DEATH "," TOTAL_RECOVERED  "]"
	ACTIVE_BUF=ACTIVE_BUF "," "[" "\"" a[22] "\"" "," ACTIVE_CASES "]"

}
END {
	#print TOTAL_BUF
	#print ACTIVE_BUF
	print "<!-- BEG NEW CHART -->"
	#
	print "<script type=\"text/javascript\">"
	print "      google.charts.load(\"current\", {\"packages\":[\"corechart\"]});"
	print "      google.charts.setOnLoadCallback(drawChart);"
	print ""
	print "      function drawChart() {"
	print "        var data = google.visualization.arrayToDataTable(["
	print "          [\"Day\", \"Active\", \"Deaths\", \"Recovered\"]"
	#
	print TOTAL_BUF
	#
	print "        ]);"
	print "        var options_fullStacked = {"
	print "          isStacked: \"percent\","
	print "          height: 300,"
	print "          legend: {position: \"top\", maxLines: 3},"
	print "          vAxis: {"
	print "            minValue: 0"
	print "          }"
	print "        };"
	print "        var chart = new google.visualization.AreaChart(document.getElementById(\"total_cases_chart\"));"
	print "        chart.draw(data, options_fullStacked);"
	print "}"
	print "</script>"
	#
	print "<script type=\"text/javascript\">"
	print "      google.charts.load(\"current\", {\"packages\":[\"corechart\"]});"
	print "      google.charts.setOnLoadCallback(drawChart);"
	print "      function drawChart() {"
	print "        var data = google.visualization.arrayToDataTable(["
	print "          [\"Day\", \"Active Cases\"]"
	#
	print ACTIVE_BUF
	#
	print "        ]);"
	print "        var options = {"
	print "          curveType: \"function\","
	print "          legend: { position: \"top\" },"
	print "	  width: 300,"
	print "	  height: 300"
	print "        };"
	print "        var chart = new google.visualization.LineChart(document.getElementById(\"active_cases_chart\"));"
	print "        chart.draw(data, options);"
	print "      }"
	print "</script>"
	print "<!-- END NEW CHART -->"
}

