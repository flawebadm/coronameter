#!/bin/bash
ulimit -s 65536
FILELIST=`grep -c "^|" prepare/ff.* | grep -v ":0$"`
for i in `echo "$FILELIST"`
do
	echo "... Working on $i"
	f=`basename $i`
	FLE=`echo "$f" | awk -F":" '{ print $1; }'`
	CNT=`echo "$f" | awk -F":" '{ print $2; }'`
	#echo "... FLE=$FLE CNT=$CNT"
	if [ $CNT -lt 200 ]
	then
		#MYCMD="grep -v \"^|\"  prepare/$FLE >  CORR.PREPARE/$FLE"
		#echo "... MYCMD=$MYCMD"
		touch CORR.PREPARE/$FLE
		grep -v "^|"  prepare/$FLE > CORR.PREPARE/$FLE
		WCL=`wc -l  CORR.PREPARE/$FLE| awk '{ print $1; }'`
		#echo "... $WCL"
		if [ $WCL -gt 0 ]
		then	
			cp CORR.PREPARE/$FLE prepare/$FLE
		else
			rm prepare/$FLE
		fi
	else
		#MYCMD="mv prepare/$FLE CORR.PREPARE"
		#echo "... MYCMD=$MYCMD"
		mv prepare/$FLE CORR.PREPARE/
	fi
done

