BEGIN {
	BUF=""
}
{
	if(NF == 26) {
		print $0
	} else {
		split($0,a);
		if(NF > 26) {
			split($0,x)
			SKIPCOL=21+(NF-26)
                	for(i=1;i<=12;i++) {
                        	a[i]=x[i]
                	}
                	for(j=SKIPCOL;j<=NF;j++) {
                        	a[i++]=x[j]
                	}
		} else {
			# NF < 26
			split($0,x)
			for(i=1;i<=NF-6;i++) {
				a[i]=x[i]
			}
			for(i=NF-5;i<=20;i++) {
				a[i]=""
			}
			for(j=NF-5;j<=NF; j++) {
				a[i++]=x[j]
			}
		}
		BUF=a[1]
		for(i=2;i<=26;i++) {
			BUF=BUF V_CSV_SEPARATOR a[i]
		}
		print BUF
	}
}
END {
}
