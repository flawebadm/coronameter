#!/bin/bash
# ff
#
ulimit -s 65536
. $PWD/000profile
RET=$?
if [ $RET -ne 0 ]
then
	echo "ERROR: LINENO=$LINENO RET=$RET PROFILE=$PWD/000profile. Exiting."
	exit $RET
fi

function html_head {
#ff#echo "LINENO=$LINENO"
	INFILE="$1"
	OUTFILE="$2"
	echo "<head>	
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
<title>Coronometer V8.4c by ff.</title>
<!-- NEW CHART -->
<script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>
<!-- NEW CHART -->
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  # width: 100%;
}

th.tleft {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 2px;
  background-color: #dddddd;
}

td.tleft {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 2px;
}

th {
  border: 1px solid #dddddd;
  text-align: right;
  padding: 2px;
  background-color: #dddddd;
}

td.noborder {
  border: 0px solid #dddddd;
  text-align: right;
  padding: 2px;
}

td.tleftnoborder {
  border: 0px solid #dddddd;
  text-align: left;
  padding: 2px;
}


td {
  border: 1px solid #dddddd;
  text-align: right;
  padding: 2px;
}

.btn {
  border: none;
  color: white;
  padding: 14px 28px;
  font-size: 16px;
  cursor: pointer;
}

.btn1 {
  border: none;
  color: white;
  padding: 7px 14px;
  font-size: 16px;
  cursor: pointer;
}

.success {background-color: #4CAF50;} /* Green */
.success:hover {background-color: #46a049;}

.info {background-color: #2196F3;} /* Blue */
.info:hover {background: #0b7dda;}

.warning {background-color: #ff9800;} /* Orange */
.warning:hover {background: #e68a00;}

.danger {background-color: #f44336;} /* Red */ 
.danger:hover {background: #da190b;}

.default {background-color: #e7e7e7; color: black;} /* Gray */ 
.default:hover {background: #ddd;}

#tr:nth-child(even) {
#  background-color: #dddddd;
#}
</style>
</head>"
} 

function html_page {
#ff#echo "LINENO=$LINENO"
	INFILE="$1"
	OUTFILE="$2"
	echo "<!DOCTYPE html>"
	echo "<html>"
#ff#echo "LINENO=$LINENO"
	html_head
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET. Exiting."
		exit $RET
	fi
#ff#echo "LINENO=$LINENO"
	html_body
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET. Exiting."
		exit $RET
	fi
	echo "</html>"
}

function html_page_country {
#ff#echo "LINENO=$LINENO"
	INFILE="$1"
	OUTFILE="$2"
	FILELIST=""
	#
	##ff##FILELIST=`ls -Xr $PREPAREDIR/$SOURCENAME.country.day.prepared.* | egrep ".Albania|.Australia|.Austria|.Belarus|.Belgium|.BosniaandHerzegovina|.Brazil|.Bulgaria|.China|.Canada|.Croatia|.Czechia|.Denmark|.Finland|.France|.Germany|.Greece|.HongKong|.Hungary|.Iran|.Israel|.Italy|.Japan|.Montenegro|.NorthMacedonia|.Netherlands|.Peru|.Poland|.Romania|.Russia|.Serbia|.Singapore|.Slovakia|.Slovenia|.Spain|.Sweden|.Switzerland|.S.Korea|.Taiwan|.Turkey|.UK|.USA|.Vietnam|.WORLD"`
	FILELIST=`ls -Xr $PREPAREDIR/$SOURCENAME.country.day.prepared.* | egrep ".Albania|.Austria|.BosniaandHerzegovina|.Bulgaria|.Croatia|.Czechia|.Germany|.Greece|.Hungary|.Italy|.Montenegro|.NorthMacedonia|.Romania|.Serbia|.Slovakia|.Slovenia|.Switzerland|.WORLD"`
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET FILELIST=$FILELIST. Exiting."
		exit $RET
	else
		FILECOUNT=`echo "$FILELIST" | grep -v "^$" | wc -l`
		RET=$?
		if [ $RET -ne 0 ]
		then
			echo "ERROR: LINENO=$LINENO Country RET=$RET FILECOUNT=$FILECOUNT. Exiting."
			exit $RET
		fi
		#
		#
		#
		for i in `echo "$FILELIST"`
		do
			echo "<!-- Working on $i ... -->"
			INFILE="$i"
			INBASE=$(basename -- "$INFILE")
			INEXTENSION="${INBASE##*.}"
			OUTFILE="htdocsC/country.day.$INEXTENSION.html"
			MODYINFILE="$TEMPDIR/$INBASE.mody.temp"
			SORTINFILE="$TEMPDIR/$INBASE.sort.temp"
			# echo "<!-- INFILE=$INFILE OUTFILE=$OUTFILE SORTINFILE=$SORTINFILE  ... -->"
			#
			awk -v V_CSV_SEPARATOR="$CSV_SEPARATOR" -F"$CSV_SEPARATOR" -f 822mody.awk "$INFILE" > "$MODYINFILE" 
			RET=$?
			if [ $RET -ne 0 ]
			then
				echo "ERROR: LINENO=$LINENO RET=$RET sort INFILE=$INFILE MODYINFILE=$MODYINFILE SORTINFILE=$SORTINFILE. Exiting."
				exit $RET
			fi
			sort -ru -t"$CSV_SEPARATOR" -k24 "$MODYINFILE" > "$SORTINFILE"
			RET=$?
			if [ $RET -ne 0 ]
			then
				echo "ERROR: LINENO=$LINENO RET=$RET sort INFILE=$INFILE SORTINFILE=$SORTINFILE OUTFILE=$OUTFILE. Exiting."
				exit $RET
			fi
			html_page_country_selected "$SORTINFILE" "$OUTFILE"
			RET=$?
			if [ $RET -ne 0 ]
			then
				echo "ERROR: LINENO=$LINENO RET=$RET INFILE=$INFILE SORTINFILE=$SORTINFILE OUTFILE=$OUTFILE Exiting."
				exit $RET
			fi
			rm -f "$SORTINFILE"
			rm -f "$MODYINFILE"
		#
		done
	fi
}
	
function html_page_country_selected {
#ff#echo "LINENO=$LINENO"
	INFILE="$1"
	OUTFILE="$2"
	echo "<!DOCTYPE html>" > "$OUTFILE"
	echo "<html>" >> "$OUTFILE"
	html_head "$INFILE" "$OUTFILE" >> "$OUTFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET INFILE=$INFILE OUTFILE=$OUTFILE Exiting."
		exit $RET
	fi
	html_body_country "$INFILE" "$OUTFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET INFILE=$INFILE OUTFILE=$OUTFILE Exiting."
		exit $RET
	fi
	echo "</html>" >> $OUTFILE
}
	
function body_head {
#ff#echo "LINENO=$LINENO"
	INFILE="$1"
	OUTFILE="$2"
	echo "<!-- FF body_head -->"
	echo "<big><big><strong>Coronometer</strong></big></big><small><small> (V8.4c) </small></small></br> <big><big><strong>COVID-19 Coronavirus Outbreak 2020</strong></big></big>
<br/><br><strong><big>Report: Daily collected data</big></strong>
<input type=\"button\" class=\"btn1 default\" value=\"Refresh Page\" onClick=\"window.location.reload()\"/>
<br/><br/><a href=\"#data_sources\">Data sources</a>|<a href=\"#explanation\">Explanation</a>|<a href=\"#disclaimer\">Disclaimer</a>"
}

function body_head_country {
	INFILE="$1"
	OUTFILE="$2"
	echo "<!-- FF body_head -->"
	echo "<big><big><strong>Coronometer</strong></big></big><small><small> (V8.4c) </small></small></br><big><big><strong>COVID-19 Coronavirus Outbreak 2020</strong></big></big>
<br/><br><strong><big>Report: Data collected by country</big></strong>
<input type=\"button\" class=\"btn1 default\" value=\"Refresh Page\" onClick=\"window.location.reload()\"/>
<br><strong><big><a href="index.html">View daily collected data</a></big></strong>
<br/><br/><a href=\"#data_sources\">Data sources</a>|<a href=\"#explanation\">Explanation</a>|<a href=\"#disclaimer\">Disclaimer</a>"
}

function body_body_country {
	INFILE="$1"
	OUTFILE="$2"
	echo "<!-- FF body_body_country -->"
	awk -F"$CSV_SEPARATOR" -f 840htmldaycountry.awk "$INFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET INFILE=$INFILE OUTFILE=$OUTFILE. Exiting."
		exit $RET
	fi
}

function body_body {
#ff#echo "LINENO=$LINENO"
	INFILE="$1"
	OUTFILE="$2"
	echo "<!-- FF body_body -->"
	FILELIST=""
	FIRST_FILE=1
	#
	FILELIST=`ls -Xr $PREPAREDIR/$SOURCENAME.day.prepared.*`
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET FILELIST=$FILELIST. Exiting."
		exit $RET
	else
		FILECOUNT=`echo "$FILELIST" | grep -v "^$" | wc -l`
		RET=$?
		if [ $RET -ne 0 ]
		then
			echo "ERROR: LINENO=$LINENO Country RET=$RET FILECOUNT=$FILECOUNT. Exiting."
			exit $RET
		fi
		#
		#
		#
		for i in `echo "$FILELIST"`
		do
			echo "<!-- Working on $i ... -->"
			INFILE="$i"
			INBASE=$(basename -- "$INFILE")
			INEXTENSION="${INBASE##*.}"
			INFILETEMP="$TEMPDIR/$INBASE.temp"
			INFILETEMP2="$TEMPDIR/$INBASE.2.temp"
			#
			#ff#egrep "^Albania|^Australia|^Austria|^Belarus|^Belgium|^Bosnia and Herzegovina|^Brazil|^Bulgaria|^China|^Canada|^Croatia|^Czechia|^Denmark|^Finland|^France|^Germany|^Greece|^Hong Kong|^Hungary|^Iran|^Israel|^Italy|^Japan|^Montenegro|^North Macedonia|^Netherlands|^Peru|^Poland|^Romania|^Russia|^Serbia|^Singapore|^Slovakia|^Slovenia|^Spain|^Sweden|^Switzerland|^S. Korea|^Taiwan|^Turkey|^UK|^USA|^Vietnam" "$INFILE" | sort  -t"$CSV_SEPARATOR" -k2nr > "$INFILETEMP"
			egrep "^Albania|^Austria|^Bosnia and Herzegovina|^Bulgaria|^Croatia|^Czechia|^Germany|^Greece|^Hungary|^Italy|^Montenegro|^North Macedonia|^Romania|^Serbia|^Slovakia|^Slovenia|^Switzerland" "$INFILE" | sort  -t"$CSV_SEPARATOR" -k2nr > "$INFILETEMP"
			egrep "^World|^Total:" "$INFILE" | sort -u >> "$INFILETEMP"
			RET=$?
			if [ $RET -ne 0 ]
			then
				echo "ERROR: LINENO=$LINENO RET=$RET egrep INFILE=$INFILE INFILETEMP=$INFILETEMP. Exiting."
				exit $RET
			fi
			#
			LEFTDAY="country.day"
			awk -v V_FIRST_FILE="$FIRST_FILE" -v V_LEFTDAY="$LEFTDAY" -F"$CSV_SEPARATOR" -f 820htmlday.awk "$INFILETEMP"
			#
			if [ "$FIRST_FILE" -eq 1 ]
			then
				egrep -v "^World|^Total:" "$INFILETEMP" | sort -u | awk -v V_CSV_SEPARATOR="$CSV_SEPARATOR" -F"$CSV_SEPARATOR" -f 825data.awk > "$INFILETEMP2"
				RET=$?
				if [ $RET -ne 0 ]
				then
					echo "ERROR: LINENO=$LINENO RET=$RET INFILETEMP=$INFILETEMP INFILETEMP2=$INFILETEMP2. Exiting."
					exit $RET
				fi
				sort "$INFILETEMP2" -t"$CSV_SEPARATOR" -rn --key=2 | head -10 | awk -v V_COLNUM1=1 -v V_COLNUM2=2 -v V_COLTITLE1="Country" -v V_COLTITLE2="Total Cases" -v V_CHARTTITLE="Total Cases" -v V_CHARTDIV="chart_total_cases" -F"$CSV_SEPARATOR" -f 826barchart.awk
				sort "$INFILETEMP2" -t"$CSV_SEPARATOR" -rn --key=4 | head -10 | awk -v V_COLNUM1=1 -v V_COLNUM2=4 -v V_COLTITLE1="Country" -v V_COLTITLE2="Total Deaths" -v V_CHARTTITLE="Total Deaths" -v V_CHARTDIV="chart_total_deaths" -F"$CSV_SEPARATOR" -f 826barchart.awk
				sort "$INFILETEMP2" -t"$CSV_SEPARATOR" -rn --key=6 | head -10 | awk -v V_COLNUM1=1 -v V_COLNUM2=6 -v V_COLTITLE1="Country" -v V_COLTITLE2="Total Recovered" -v V_CHARTTITLE="Total Recovered" -v V_CHARTDIV="chart_total_recovered" -F"$CSV_SEPARATOR" -f 826barchart.awk
				sort "$INFILETEMP2" -t"$CSV_SEPARATOR" -rn --key=7 | head -10 | awk -v V_COLNUM1=1 -v V_COLNUM2=7 -v V_COLTITLE1="Country" -v V_COLTITLE2="Active Cases" -v V_CHARTTITLE="Active Cases" -v V_CHARTDIV="chart_active_cases" -F"$CSV_SEPARATOR" -f 826barchart.awk
				sort "$INFILETEMP2" -t"$CSV_SEPARATOR" -rn --key=8 | head -10 | awk -v V_COLNUM1=1 -v V_COLNUM2=8 -v V_COLTITLE1="Country" -v V_COLTITLE2="Serious, Critical" -v V_CHARTTITLE="Serious, Critical" -v V_CHARTDIV="chart_serious_critical" -F"$CSV_SEPARATOR" -f 826barchart.awk
				sort "$INFILETEMP2" -t"$CSV_SEPARATOR" -rn --key=9 | head -10 | awk -v V_COLNUM1=1 -v V_COLNUM2=9 -v V_COLTITLE1="Country" -v V_COLTITLE2="Total Cases/ 1M pop" -v V_CHARTTITLE="Total Cases/ 1M pop" -v V_CHARTDIV="chart_total_cases_1m_pop" -F"$CSV_SEPARATOR" -f 826barchart.awk
				sort "$INFILETEMP2" -t"$CSV_SEPARATOR" -rn --key=10 | head -10 | awk -v V_COLNUM1=1 -v V_COLNUM2=10 -v V_COLTITLE1="Country" -v V_COLTITLE2="Deaths/ 1M pop" -v V_CHARTTITLE="Deaths/ 1M pop" -v V_CHARTDIV="chart_deaths_1m_pop" -F"$CSV_SEPARATOR" -f 826barchart.awk
				sort "$INFILETEMP2" -t"$CSV_SEPARATOR" -rn --key=12 | head -10 | awk -v V_COLNUM1=1 -v V_COLNUM2=12 -v V_COLTITLE1="Country" -v V_COLTITLE2="Tests/ 1M pop" -v V_CHARTTITLE="Tests/ 1M pop" -v V_CHARTDIV="chart_tests_1m_pop" -F"$CSV_SEPARATOR" -f 826barchart.awk
				#	
				rm -f "$INFILETEMP"
				rm -f "$INFILETEMP2"
				#	
				FIRST_FILE=0
			fi
		#
		#
		done
	fi
}

function body_foot {
#ff#echo "LINENO=$LINENO"
	INFILE="$1"
	OUTFILE="$2"
	echo "<!-- FF body_foot -->"
echo "<strong><span id=\"data_sources\">Data Sources</span></strong>
<br/>https://www.who.int/ 
<br/>https://coronavirus.jhu.edu/ 
<br/>https://ourworldindata.org 
<br/>https://www.worldometers.info/ 
</td>
</tr>
<tr>
<td class=\"tleftnoborder\">
<strong><span id=\"explanation\">Explanation</span></strong> 
<br/>+ Data are collected and compared with previous data every 10 minutes.
<br/>+ If differences are found this new data is prepared for publishing. 
<br/>+ Every 30-60 minutes all data prepared for publishing will be HTML formatted and via FTP transferred to web server. 
<br/>+ It is important to understand that different countries and organization reports the COVID-19 numbers using different methodologies and reporting interval, 
<br/>therefore you can experience differences in my report with the data which you have access locally, 
<br/>but in long term (about 24-48 hours) the data will be more than 90% accurate. 
<br/>+ The values in columns New Cases and New Deaths are differences between current and previous collected data and for use in my future analysis.
</td>
</tr>
<tr>
<td class=\"tleftnoborder\">
<strong><span id=\"disclaimer\">Disclaimer</span></strong> 
<br/>All errors belongs to me. Flavio. 
<br/>COPYRIGHT (c) 2020 by ff: You have right to copy whatewer you want."
}

function html_body {
#ff#echo "LINENO=$LINENO"
	INFILE="$1"
	OUTFILE="$2"
	echo "<!-- FF body  -->
<body>"
	echo "<table>"
	echo "<tr> <td class=\"tleftnoborder\">"
	body_head
	echo "</td></tr>"
	echo "<tr> <td class=\"tleftnoborder\">"
	body_body
	echo "<tr> <td class=\"tleftnoborder\">"
	body_foot
	echo "</td></tr>"
	echo "</table>"
	echo "</body>"
}

function html_body_country {
#ff#echo "LINENO=$LINENO"
	INFILE="$1"
	OUTFILE="$2"
	echo "<!-- FF body  -->
<body>" >> "$OUTFILE"
	echo "<table>" >> "$OUTFILE"
	echo "<tr> <td class=\"tleftnoborder\">" >> "$OUTFILE"
	body_head_country "$INFILE" "$OUTFILE" >> "$OUTFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET INFILE=$INFILE OUTFILE=$OUTFILE Exiting."
		exit $RET
	fi
	echo "</td></tr>" >> "$OUTFILE"
	echo "<tr> <td class=\"tleftnoborder\">" >> "$OUTFILE"
	body_body_country "$INFILE" "$OUTFILE" >> "$OUTFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET INFILE=$INFILE OUTFILE=$OUTFILE Exiting."
		exit $RET
	fi
	echo "<tr> <td class=\"tleftnoborder\">" >> "$OUTFILE"
	body_foot "$INFILE" "$OUTFILE" >> "$OUTFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET INFILE=$INFILE OUTFILE=$OUTFILE Exiting."
		exit $RET
	fi
	echo "</td></tr>" >> "$OUTFILE"
	echo "</table>" >> "$OUTFILE"
	#
	# chart scripts
	#
	SORTINFILE2="$INFILE.2"
	sort --field-separator="$CSV_SEPARATOR" --key=24 "$INFILE" > "$SORTINFILE2"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET sort INFILE=$INFILE SORTINFILE2=$SORTINFILE2. Exiting."
		exit $RET
	fi
	#########
	## old ## awk -F"$CSV_SEPARATOR" -f 824prepchart.awk "$SORTINFILE2" >> "$OUTFILE"
	#########
	awk -v V_TRENDTYPE="exponential" -v V_COLTITLE1="Total Cases" -v V_CHARTDIV="total_cases_chart" -v V_COLNUM1=2 -F"$CSV_SEPARATOR" -f 827linetrend.awk "$SORTINFILE2" >> "$OUTFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET awk 827linetrend.awk  SORTINFILE2=$SORTINFILE2. Exiting."
		exit $RET
	fi
	awk -v V_TRENDTYPE="exponential" -v V_COLTITLE1="Total Deaths" -v V_CHARTDIV="total_deaths_chart" -v V_COLNUM1=4 -F"$CSV_SEPARATOR" -f 827linetrend.awk "$SORTINFILE2" >> "$OUTFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET awk 827linetrend.awk  SORTINFILE2=$SORTINFILE2. Exiting."
		exit $RET
	fi
	awk -v V_TRENDTYPE="exponential" -v V_COLTITLE1="Total Recovered" -v V_CHARTDIV="total_recovered_chart" -v V_COLNUM1=6 -F"$CSV_SEPARATOR" -f 827linetrend.awk "$SORTINFILE2" >> "$OUTFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET awk 827linetrend.awk  SORTINFILE2=$SORTINFILE2. Exiting."
		exit $RET
	fi
	awk -v V_TRENDTYPE="exponential" -v V_COLTITLE1="Active Cases" -v V_CHARTDIV="active_cases_chart" -v V_COLNUM1=7 -F"$CSV_SEPARATOR" -f 827linetrend.awk "$SORTINFILE2" >> "$OUTFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET awk 827linetrend.awk  SORTINFILE2=$SORTINFILE2. Exiting."
		exit $RET
	fi
	awk -v V_TRENDTYPE="exponential" -v V_COLTITLE1="Serious, Critical" -v V_CHARTDIV="serious_critical_chart" -v V_COLNUM1=8 -F"$CSV_SEPARATOR" -f 827linetrend.awk "$SORTINFILE2" >> "$OUTFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET awk 827linetrend.awk  SORTINFILE2=$SORTINFILE2. Exiting."
		exit $RET
	fi
	awk -v V_TRENDTYPE="exponential" -v V_COLTITLE1="Total Cases 1M pop" -v V_CHARTDIV="total_cases_1m_pop_chart" -v V_COLNUM1=9 -F"$CSV_SEPARATOR" -f 827linetrend.awk "$SORTINFILE2" >> "$OUTFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET awk 827linetrend.awk  SORTINFILE2=$SORTINFILE2. Exiting."
		exit $RET
	fi
	awk -v V_TRENDTYPE="exponential" -v V_COLTITLE1="New Cases" -v V_CHARTDIV="new_cases_chart" -v V_COLNUM1=3 -F"$CSV_SEPARATOR" -f 827linetrend.awk "$SORTINFILE2" >> "$OUTFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET awk 827linetrend.awk  SORTINFILE2=$SORTINFILE2. Exiting."
		exit $RET
	fi
	awk -v V_TRENDTYPE="exponential" -v V_COLTITLE1="New Deaths" -v V_CHARTDIV="new_deaths_chart" -v V_COLNUM1=5 -F"$CSV_SEPARATOR" -f 827linetrend.awk "$SORTINFILE2" >> "$OUTFILE"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "ERROR: LINENO=$LINENO RET=$RET awk 827linetrend.awk  SORTINFILE2=$SORTINFILE2. Exiting."
		exit $RET
	fi
	#
	rm -f "$SORTINFILE2"
	#
	#
	#
	echo "</body>" >> "$OUTFILE"
}

#
# main execute
#
#ff#echo "LINENO=$LINENO"
rm -f $TEMPDIR/*.temp
html_page > htdocsC/index.html
RET=$?
if [ $RET -ne 0 ]
then
	echo "ERROR: LINENO=$LINENO RET=$RET INFILE=$INFILE OUTFILE=$OUTFILE Exiting."
	exit $RET
fi
#
#ff#echo "LINENO=$LINENO"
html_page_country 
RET=$?
if [ $RET -ne 0 ]
then
	echo "ERROR: LINENO=$LINENO RET=$RET INFILE=$INFILE OUTFILE=$OUTFILE Exiting."
	exit $RET
fi
rm -f $TEMPDIR/*.temp

