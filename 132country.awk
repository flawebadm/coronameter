BEGIN {
}
{ 
# main beg
	COUNTRY=$1
	#
	gsub(" ","",COUNTRY);
	gsub("\\.","_",COUNTRY);
	sub("World","WORLD",COUNTRY);
	sub("Total:","WORLD",COUNTRY);
	FILEDAY=V_LEFTDAY "." COUNTRY 
	#
	print $0 >> FILEDAY
	#
#main end
}
END {
}
